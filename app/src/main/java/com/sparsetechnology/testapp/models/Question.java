package com.sparsetechnology.testapp.models;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Question {
    public enum TYPE {TEST, SURVEY}

    private TYPE type;
    private String text;
    private String[] choices;
    private int[] answers;

    public static Question newInstance() {
        Question question = new Question();
        question.type = TYPE.TEST;
        question.text = "";
        question.choices = new String[0];
        question.answers = new int[0];

        return question;
    }

    public static Question newInstance(TYPE type, String text, String[] choices, int[] answers) {
        Question question = new Question();
        question.type = type;
        question.text = text;
        question.choices = choices;
        question.answers = answers;

        return question;
    }

    public static Question newInstanceFromJson(String json) {
        try {
            Question question = new Question();

            JSONObject object = new JSONObject(json);
            question.setType(object.getString("type").equals("test") ? TYPE.TEST : TYPE.SURVEY);
            question.setText(object.getString("text"));

            JSONArray arrayC = object.getJSONArray("choices");
            String[] strings = new String[arrayC.length()];
            for (int i = 0; i < arrayC.length(); i++)
                strings[i] = arrayC.getString(i);

            JSONArray arrayA = object.getJSONArray("answers");
            int[] values = new int[arrayA.length()];
            for (int i = 0; i < arrayA.length(); i++)
                values[i] = arrayA.getInt(i);

            question.setChoices(strings);
            question.setAnswer(values);

            return question;
        } catch (JSONException ignored) {
        }

        return newInstance();
    }

    public TYPE getType() {
        return type;
    }

    public void setType(TYPE type) {
        this.type = type;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String[] getChoices() {
        return choices;
    }

    public void setChoices(String[] choices) {
        this.choices = choices;
    }

    public int[] getAnswer() {
        return answers;
    }

    public void setAnswer(int[] answers) {
        this.answers = answers;
    }

    @Override
    public String toString() {
        try {
            JSONObject object = new JSONObject();
            object.put("type", this.type == TYPE.TEST ? "test" : "survey");
            object.put("text", this.text);

            JSONArray arrayC = new JSONArray();
            for (String c : this.choices)
                arrayC.put(c);

            object.put("choices", arrayC);

            JSONArray arrayA = new JSONArray();
            for (int i : this.answers)
                arrayA.put(i);

            object.put("answer", arrayA);

            return object.toString();
        } catch (JSONException ignored) {
        }

        return super.toString();
    }
}
