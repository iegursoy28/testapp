package com.sparsetechnology.testapp.ui;

import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;

import com.sparsetechnology.testapp.R;
import com.sparsetechnology.testapp.models.Question;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {
    @BindView(R.id.main_question_container)
    public FrameLayout mainContainer;

    private ArrayList<Question> questions = new ArrayList<>();
    private Question currentQuestion = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        crateSampleQuestions();
    }

    private void crateSampleQuestions() {
        questions.add(Question.newInstance(Question.TYPE.TEST, "Soru 1", new String[]{"cevap_1", "cevap_2", "cevap_3", "cevap_4", "cevap_5"}, new int[]{0}));
        questions.add(Question.newInstance(Question.TYPE.TEST, "Soru 2", new String[]{"cevap_1", "cevap_2", "cevap_3", "cevap_4", "cevap_5"}, new int[]{2}));
        questions.add(Question.newInstance(Question.TYPE.TEST, "Soru 3", new String[]{"cevap_1", "cevap_2", "cevap_3", "cevap_4", "cevap_5"}, new int[]{1}));
        questions.add(Question.newInstance(Question.TYPE.TEST, "Soru 4", new String[]{"cevap_1", "cevap_2", "cevap_3", "cevap_4", "cevap_5"}, new int[]{3}));
        questions.add(Question.newInstance(Question.TYPE.SURVEY, "Soru 5", new String[]{"cevap_1", "cevap_2", "cevap_3", "cevap_4", "cevap_5"}, new int[]{0, 1, 2}));

        setQuestion(questions.get(0));
    }

    @OnClick({R.id.main_btn_next, R.id.main_btn_pre})
    public void onClickBtn(View view) {
        if (currentQuestion == null)
            return;

        int index = questions.indexOf(currentQuestion);
        switch (view.getId()) {
            case R.id.main_btn_next:
                if (index + 1 >= questions.size())
                    return;
                setQuestion(questions.get(index + 1));
                break;
            case R.id.main_btn_pre:
                if (index == 0)
                    return;
                setQuestion(questions.get(index - 1));
                break;
        }
    }

    private void setQuestion(final Question question) {
        QuestionFragment questionFragment = new QuestionFragment();

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.main_question_container, questionFragment).commit();

        questionFragment.SetQuestion(question);
        currentQuestion = question;
    }
}
