package com.sparsetechnology.testapp.ui;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sparsetechnology.testapp.R;
import com.sparsetechnology.testapp.models.Question;

import java.util.ArrayList;
import java.util.Arrays;

public class QuestionFragment extends Fragment {

    private Question question;
    private ChoicesAdapter choicesAdapter;

    private TextView txtQuestion;
    private RecyclerView recyclerChoicesView;

    public QuestionFragment() {
        choicesAdapter = new ChoicesAdapter();
    }

    public void SetQuestion(@NonNull final Question question) {
        this.question = question;

        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                txtQuestion.setText(question.getText());
                choicesAdapter.setDataSet(new ArrayList<String>(Arrays.asList(question.getChoices())));
            }
        });
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_question, container, false);

        txtQuestion = view.findViewById(R.id.q_text);
        recyclerChoicesView = view.findViewById(R.id.q_choices);

        recyclerChoicesView.setItemAnimator(new DefaultItemAnimator());
        recyclerChoicesView.setHasFixedSize(true);
        recyclerChoicesView.setAdapter(choicesAdapter);

        return view;
    }

    public static class ChoicesAdapter extends RecyclerView.Adapter<ChoicesAdapter.ChoiceHolder> {

        private ArrayList<String> dataSet = new ArrayList<>();

        @NonNull
        @Override
        public ChoiceHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.view_choice_row, viewGroup, false);
            ChoiceHolder holder = new ChoiceHolder(view);

            return holder;
        }

        @Override
        public void onBindViewHolder(@NonNull ChoiceHolder choiceHolder, int i) {
            choiceHolder.txtOrder.setText(String.format("%s) ", i));
            choiceHolder.txtText.setText(dataSet.get(i));
        }

        @Override
        public int getItemCount() {
            return dataSet.size();
        }

        public void setDataSet(ArrayList<String> dataSet) {
            this.dataSet = dataSet;
            notifyDataSetChanged();
        }

        public static class ChoiceHolder extends RecyclerView.ViewHolder {
            public TextView txtOrder;
            public TextView txtText;

            public ChoiceHolder(@NonNull View itemView) {
                super(itemView);
                txtOrder = itemView.findViewById(R.id.c_order);
                txtText = itemView.findViewById(R.id.c_text);
            }
        }
    }
}
